# base image
FROM node:12.2.0-alpine

EXPOSE 80

RUN apk add nginx && mkdir /run/nginx && cd /var/www/localhost/htdocs;

ADD config/default.conf /etc/nginx/conf.d/default.conf

# set working directory
WORKDIR /var/www/localhost/htdocs

# add `/app/node_modules/.bin` to $PATH
# ENV PATH /app/node_modules/.bin:$PATH

# install and cache app dependencies
COPY ./bundle /var/www/localhost/htdocs

# start app
CMD ["/bin/sh", "-c", "exec nginx -g 'daemon off;';"]