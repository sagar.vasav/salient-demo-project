/*test('There is "E" in Hello World', () => {
  expect('Hello World').not.toMatch(/e/);
});*/

test('There is "ello" in Hello World', () => {
  expect('Hello World').toMatch(/ello/);
});

test('There is no "I" in Hello World', () => {
  expect('Hello World').not.toMatch(/I/);
});